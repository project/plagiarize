<?php
/**
 * @file
 * Template file for the plagiarize teaser to be embedded into a node body.
 */
?>
<div class="embedded-summary embedded-summary-id-<?php print $node->nid; ?> embedded-summary-type-<?php print $node->type; ?> node<?php print ($node->status == 0 ? '-unpublished' : ''); ?>">
  <h2 id="<?php print $node->uniqueid; ?>"><?php print $title; ?></h2>
  <div class="summary">
    <?php print $summary; ?>
  </div>
</div>
